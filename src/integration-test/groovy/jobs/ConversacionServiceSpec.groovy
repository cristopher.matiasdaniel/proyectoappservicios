package jobs

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ConversacionServiceSpec extends Specification {

    ConversacionService conversacionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Conversacion(...).save(flush: true, failOnError: true)
        //new Conversacion(...).save(flush: true, failOnError: true)
        //Conversacion conversacion = new Conversacion(...).save(flush: true, failOnError: true)
        //new Conversacion(...).save(flush: true, failOnError: true)
        //new Conversacion(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //conversacion.id
    }

    void "test get"() {
        setupData()

        expect:
        conversacionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Conversacion> conversacionList = conversacionService.list(max: 2, offset: 2)

        then:
        conversacionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        conversacionService.count() == 5
    }

    void "test delete"() {
        Long conversacionId = setupData()

        expect:
        conversacionService.count() == 5

        when:
        conversacionService.delete(conversacionId)
        sessionFactory.currentSession.flush()

        then:
        conversacionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Conversacion conversacion = new Conversacion()
        conversacionService.save(conversacion)

        then:
        conversacion.id != null
    }
}
