package jobs

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class RegistroPagoServiceSpec extends Specification {

    RegistroPagoService registroPagoService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new RegistroPago(...).save(flush: true, failOnError: true)
        //new RegistroPago(...).save(flush: true, failOnError: true)
        //RegistroPago registroPago = new RegistroPago(...).save(flush: true, failOnError: true)
        //new RegistroPago(...).save(flush: true, failOnError: true)
        //new RegistroPago(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //registroPago.id
    }

    void "test get"() {
        setupData()

        expect:
        registroPagoService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<RegistroPago> registroPagoList = registroPagoService.list(max: 2, offset: 2)

        then:
        registroPagoList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        registroPagoService.count() == 5
    }

    void "test delete"() {
        Long registroPagoId = setupData()

        expect:
        registroPagoService.count() == 5

        when:
        registroPagoService.delete(registroPagoId)
        sessionFactory.currentSession.flush()

        then:
        registroPagoService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        RegistroPago registroPago = new RegistroPago()
        registroPagoService.save(registroPago)

        then:
        registroPago.id != null
    }
}
