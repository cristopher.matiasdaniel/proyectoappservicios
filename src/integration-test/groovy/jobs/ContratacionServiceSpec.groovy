package jobs

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ContratacionServiceSpec extends Specification {

    ContratacionService contratacionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Contratacion(...).save(flush: true, failOnError: true)
        //new Contratacion(...).save(flush: true, failOnError: true)
        //Contratacion contratacion = new Contratacion(...).save(flush: true, failOnError: true)
        //new Contratacion(...).save(flush: true, failOnError: true)
        //new Contratacion(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //contratacion.id
    }

    void "test get"() {
        setupData()

        expect:
        contratacionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Contratacion> contratacionList = contratacionService.list(max: 2, offset: 2)

        then:
        contratacionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        contratacionService.count() == 5
    }

    void "test delete"() {
        Long contratacionId = setupData()

        expect:
        contratacionService.count() == 5

        when:
        contratacionService.delete(contratacionId)
        sessionFactory.currentSession.flush()

        then:
        contratacionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Contratacion contratacion = new Contratacion()
        contratacionService.save(contratacion)

        then:
        contratacion.id != null
    }
}
