package jobs

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TrabajadorServiceSpec extends Specification {

    TrabajadorService trabajadorService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Trabajador(...).save(flush: true, failOnError: true)
        //new Trabajador(...).save(flush: true, failOnError: true)
        //Trabajador trabajador = new Trabajador(...).save(flush: true, failOnError: true)
        //new Trabajador(...).save(flush: true, failOnError: true)
        //new Trabajador(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //trabajador.id
    }

    void "test get"() {
        setupData()

        expect:
        trabajadorService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Trabajador> trabajadorList = trabajadorService.list(max: 2, offset: 2)

        then:
        trabajadorList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        trabajadorService.count() == 5
    }

    void "test delete"() {
        Long trabajadorId = setupData()

        expect:
        trabajadorService.count() == 5

        when:
        trabajadorService.delete(trabajadorId)
        sessionFactory.currentSession.flush()

        then:
        trabajadorService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Trabajador trabajador = new Trabajador()
        trabajadorService.save(trabajador)

        then:
        trabajador.id != null
    }
}
