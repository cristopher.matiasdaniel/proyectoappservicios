<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="tuerquitas.jpg" type="image/x-ico"/>

    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation">
    <a class="navbar-brand" href="/#"><asset:image src="tuercas.png" alt="Grails Logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


    <g:if test="${session.SPRING_SECURITY_CONTEXT}">
	<li style="font-size: 20px; list-style: none; color:#ffffff">
    			<p> ${session.SPRING_SECURITY_CONTEXT.authentication.principal.username}</p>
    </li>
    <li style="font-size: 20px; list-style: none; color:#ffffff; margin-left: 1%">
            	<p> ${session.SPRING_SECURITY_CONTEXT.authentication.principal.authorities[0]}</p>
    </li>
     </g:if>

	
    <div class="collapse navbar-collapse" aria-expanded="false" style="height: 0.8px;" id="navbarContent">
        <ul class="nav navbar-nav ml-auto">
            <g:pageProperty name="page.nav"/>
        </ul>
    </div>

</nav>

<g:layoutBody/>

<div class="footer row" role="contentinfo">
    <div class="col">
        <a href="#">
            <asset:image src="chat.png" alt="Grails Guides" class="float-left"/>
        </a>
        <strong class="centered">Conversa con los trabajadores</strong>
        <p>Podrás hablar con los trabajadores para coordinar y preguntar todo lo que necesites</p>

    </div>
    <div class="col">
        <a href="#">
            <asset:image src="compartir.png" alt="Grails Documentation" class="float-left"/>
        </a>
        <strong class="centered">Comparte tu experiencia</strong>
        <p>Si crees que esta web podría serle útil a mas personas ¡por favor compártenos!</p>

    </div>

    <div class="col">
        <a href="#">
            <asset:image src="personas.jpg" alt="Grails Slack" class="float-left"/>
        </a>
        <strong class="centered">Mas sobre nosotros</strong>
        <p>Nuestro objetivo es facilitar el encontrar soluciones relacionadas a la entrega de servicios y entregar empleo a los que entreguen estos servicios</p>
    </div>
</div>


<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
