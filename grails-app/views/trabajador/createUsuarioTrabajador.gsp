<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'trabajador.label', default: 'Trabajador')}" />
        <title>Registro como Trabajador</title>
    </head>
    <body>
        <a href="#create-trabajador" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
            </ul>
        </div>
        <div id="create-trabajador" class="content scaffold-create" role="main">
            <h1>Registro como Trabajador</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.trabajador}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.trabajador}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.trabajador}" method="POST">
                <fieldset class="form">

                    <f:field bean="trabajador" property="usuario.nombre" />
                    <f:field bean="trabajador" property="usuario.apellido" />
                    <f:field bean="trabajador" property="usuario.correo" />
                    <f:field bean="trabajador" property="usuario.contrasenia" />
                    <f:field bean="trabajador" property="rut" />


                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="save" value="Registrarme" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
