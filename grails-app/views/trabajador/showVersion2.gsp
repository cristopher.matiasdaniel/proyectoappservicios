<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'trabajador.label', default: 'Trabajador')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <a href="#show-trabajador" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link controller="Trabajador" action="showVersion2">Perfil</g:link></li>
                <li><g:link controller="Contratacion" action="indexPorUsuario" params="[estado: "enProceso"]">Contrataciones pendientes(POR CREAR)</g:link></li>
                <li><g:link controller="Contratacion" action="indexPorUsuario" params="[estado: "terminada"]">Contrataciones finalizadas(POR CREAR)</g:link></li>
                <li><g:link controller="Trabajador" action="showVersion2">Plan (POR CREAR)</g:link></li>

            </ul>
        </div>
        <div id="show-trabajador" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="trabajador" />
            <g:form resource="${this.trabajador}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.usuario}">Modificar Perfil</g:link>
                    <input class="delete" type="submit" value="Eliminar Cuenta" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
