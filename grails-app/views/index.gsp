<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Real Services</title>
</head>
<body>
<content tag="nav">
    <!-- <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aqui va el buscador <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li class="dropdown-item"><a href="#">Environment: ${grails.util.Environment.current.name}</a></li>
            <li class="dropdown-item"><a href="#">App profile: ${grailsApplication.config.grails?.profile}</a></li>
            <li class="dropdown-item"><a href="#">App version:
                <g:meta name="info.app.version"/></a>
            </li>
            <li role="separator" class="dropdown-divider"></li>
            <li class="dropdown-item"><a href="#">Grails version:
                <g:meta name="info.app.grailsVersion"/></a>
            </li>
            <li class="dropdown-item"><a href="#">Groovy version: ${GroovySystem.getVersion()}</a></li>
            <li class="dropdown-item"><a href="#">JVM version: ${System.getProperty('java.version')}</a></li>
            <li role="separator" class="dropdown-divider"></li>
            <li class="dropdown-item"><a href="#">Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</a></li>
        </ul>
    </li> -->
	<g:if test="${session.SPRING_SECURITY_CONTEXT}">
		<g:if test="${session.SPRING_SECURITY_CONTEXT.authentication.principal.authorities[0].toString() == 'TRABAJADOR'}">
		<li class="dropdown">
            <li class="controller">
                <g:link controller="Trabajador" action="showVersion2" params="[id: 3]" style="margin-top: 20px">Perfil</g:link>
            </li>
        </li>
		<li class="dropdown">
			<li class="controller">
				<g:link controller="Servicio" action="indexListPorTrabajador" params="[trabajadorId: 3]" style="margin-top: 20px">Servicios</g:link>
			</li>
	    </li>
		</g:if>
		<g:else>
		<li class="dropdown">
        			<li class="controller">
        				<g:link controller="Usuario" action="showVersion2" params="[id: 3]" style="margin-top: 20px">Perfil</g:link>
        			</li>
        		</li>
		</g:else>
		<li class="dropdown" >
			<li class="controller">
				<g:link controller="Logout" style="margin-top: 20px">Cerrar Sesión</g:link>
			</li>
		</li>	
    </g:if>
	<g:else>
	<li class="dropdown" >
		<li class="controller">
			<g:link controller="Login" style="margin-top: 20px">Inicia Sesión</g:link>
		</li>
	</li>
	<li class="dropdown">
		<li class="controller">
			<g:link controller="Usuario" action="create" style="margin-top: 20px">Regístrate</g:link>
		</li>
	</li>
	<li class="dropdown">
		<li class="controller">
			<g:link controller="Trabajador" action="createUsuarioTrabajador" style="margin-top: 20px">Regístrate como trabajador</g:link>
		</li>
	</li>
	</g:else>
	
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administrador <span class="caret"></span></a>
        <ul class="dropdown-menu">
				<li class="controller">
					<g:link controller="Categoria">Categoria</g:link>
				</li>
				<li class="controller">
					<g:link controller="Comuna">Comuna</g:link>
				</li>
				<li class="controller">
					<g:link controller="Region">Region</g:link>
				</li>
				<li class="controller">
					<g:link controller="Estado">Estado</g:link>
				</li>
				<li class="controller">
					<g:link controller="MedioDePago">MedioDePago</g:link>
				</li>
				<li class="controller">
					<g:link controller="Direccion">Direccion</g:link>
				</li>
				<li class="controller">
					<g:link controller="Servicio">Servicio</g:link>
				</li>
				<li class="controller">
					<g:link controller="RegistroPago">RegistroPago</g:link>
				</li>
				<li class="controller">
					<g:link controller="Mensaje">Mensaje</g:link>
				</li>
				<li class="controller">
					<g:link controller="Conversacion">Conversacion</g:link>
				</li>
				<li class="controller">
					<g:link controller="Trabajador">Trabajador</g:link>
				</li>
                <li class="controller">
                    <g:link controller="Contratacion">Contratacion</g:link>
                </li>
				<li class="controller">
                    <g:link controller="Usuario">Usuario</g:link>
                </li>
        </ul>
    </li>
</content>

<div class="svg" role="presentation">
    <div class="grails-logo-container">
        <asset:image src="manos.jpg" class="grails-logo"/>
    </div>
</div>

<div id="content" role="main">
    <section class="row colset-2-its">
        <h1>Bienvenido/a a Real Services</h1>

        <p>
            Nuestro objetivo es ayudarte a encontrar personas que realicen trabajos para ti. Creemos que es necesario un canal especial para lograr una comunicación efectiva entre tu y el trabajador. Queremos darte la confianza para que no dudes de la calidad de los trabajadores que podrás encontrar.<br><br>
			¿Quieres comenzar? Te invitamos a buscar un servicio y comenzar tu primer contacto con un trabajador
        </p>
		
			<g:form controller="servicio" action="indexList" method="post" style="margin-left: 40%">
				<g:textField id="mytext" name="nombre" placeholder="Busca un servicio" value="${nombre}" role="button" aria-haspopup="true" aria-expanded="false" />
				<button id="submit-values" class="btn btn-small btn-primary" type="submit">
					<i class="icon-ok"></i>
					Buscar
				</button>
			</g:form>

        <!-- <div id="controllers" role="navigation">
            <h2>Available Controllers:</h2>
            <ul>
                <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
                    <li class="controller">
                        <g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link>
                    </li>
                </g:each>
            </ul>
        </div> -->
    </section>
</div>

</body>
</html>
