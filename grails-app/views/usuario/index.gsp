<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <content tag="nav">
                <li class="dropdown">
                        <a href="#" aria-haspopup="true" aria-expanded="false">

                <g:form controller="servicio" action="indexList" method="post">
                    <g:textField id="mytext" name="nombre" placeholder="Busca un servicio" value="${nombre}" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 15px"/>
                    <button id="submit-values" class="btn btn-small btn-primary" type="submit">
                        <i class="icon-ok"></i>
                        Buscar
                    </button>
                </g:form>
                <span class="caret"></span></a>
                </li>

                <!-- <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aqui va el buscador <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-item"><a href="#">Environment: ${grails.util.Environment.current.name}</a></li>
                        <li class="dropdown-item"><a href="#">App profile: ${grailsApplication.config.grails?.profile}</a></li>
                        <li class="dropdown-item"><a href="#">App version:
                            <g:meta name="info.app.version"/></a>
                        </li>
                        <li role="separator" class="dropdown-divider"></li>
                        <li class="dropdown-item"><a href="#">Grails version:
                            <g:meta name="info.app.grailsVersion"/></a>
                        </li>
                        <li class="dropdown-item"><a href="#">Groovy version: ${GroovySystem.getVersion()}</a></li>
                        <li class="dropdown-item"><a href="#">JVM version: ${System.getProperty('java.version')}</a></li>
                        <li role="separator" class="dropdown-divider"></li>
                        <li class="dropdown-item"><a href="#">Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</a></li>
                    </ul>
                </li> -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Perfil <span class="caret"></span></a>
                    <ul class="dropdown-menu">
            			<!-- <li class="controller">
            				<g:link controller="Usuario" action="show">Ir a mi perfil</g:link>
            			</li> -->
                        <li class="controller">
            				<g:link controller="Login">Inicia Sesión</g:link>
            			</li>
            			<li class="controller">
            				<g:link controller="Usuario">Ver Perfil</g:link>
            			</li>
            			<li class="controller">
            				<g:link controller="Logout">Cerrar Sesión</g:link>
            			</li>
                    </ul>
                </li>
            	<li class="dropdown">
            		<li class="controller">
            			<g:link controller="Usuario" action="create" style="margin-top: 20px">Regístrate</g:link>
            		</li>
            	</li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administrador <span class="caret"></span></a>
                    <ul class="dropdown-menu">
            				<li class="controller">
            					<g:link controller="Categoria">Categoria</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Comuna">Comuna</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Region">Region</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Estado">Estado</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="MedioDePago">MedioDePago</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Direccion">Direccion</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Servicio">Servicio</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="RegistroPago">RegistroPago</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Mensaje">Mensaje</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Conversacion">Conversacion</g:link>
            				</li>
            				<li class="controller">
            					<g:link controller="Trabajador">Trabajador</g:link>
            				</li>
            				<li class="controller">
                                <g:link controller="Contratacion">Contratacion</g:link>
                            </li>
                    </ul>
                </li>
            </content>
        <a href="#list-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-usuario" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${usuarioList}" />

            <div class="pagination">
                <g:paginate total="${usuarioCount ?: 0}" />
            </div>
        </div>
    </body>
</html>