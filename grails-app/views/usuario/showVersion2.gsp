<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link controller="Usuario" action="showVersion2">Perfil</g:link></li>
                <li><g:link controller="Contratacion" action="indexPorUsuario" params="[estado: "enProceso"]">Contrataciones pendientes</g:link></li>
                <li><g:link controller="Contratacion" action="indexPorUsuario" params="[estado: "terminada"]">Contrataciones finalizadas</g:link></li>
        </div>
        <div id="show-usuario" class="content scaffold-show" role="main">
            <h1>Perfil</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="usuario" />
            <g:form resource="${this.usuario}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.usuario}">Modificar Perfil</g:link>
                    <input class="delete" type="submit" value="Eliminar Cuenta" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
