<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#create-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
            </ul>
        </div>
        <div id="create-usuario" class="content scaffold-create" role="main">
            <h1><!--<g:message code="default.create.label" args="[entityName]" />-->Regístrate en Real Services</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.usuario}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.usuario}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.usuario}" method="POST">
                <fieldset class="form">
                    <f:field bean="usuario" property="nombre" />
                    <f:field bean="usuario" property="apellido" />
                    <f:field bean="usuario" property="correo" />
                    <f:field bean="usuario" property="contrasenia" />
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="save" value="Registrarme" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
