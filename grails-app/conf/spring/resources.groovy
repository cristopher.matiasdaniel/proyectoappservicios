// Place your Spring DSL code here
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder

//beans necesarios para spring security usuarios en memoria
beans = {
    userDetailsService(InMemoryUserDetailsManager,[])
    passwordEncoder(PlaintextPasswordEncoder)
}
