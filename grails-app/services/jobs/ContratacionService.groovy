package jobs

import grails.gorm.services.Service

@Service(Contratacion)
interface ContratacionService {

    Contratacion get(Serializable id)

    List<Contratacion> list(Map args)
    List<Contratacion> findAllByUsuario(Usuario usuario)
    List<Contratacion> findAllByServicio(Servicio servicio)

    Long count()

    void delete(Serializable id)

    Contratacion save(Contratacion contratacion)

}