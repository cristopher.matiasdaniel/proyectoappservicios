package jobs

import grails.gorm.services.Service

@Service(MedioDePago)
interface MedioDePagoService {

    MedioDePago get(Serializable id)

    List<MedioDePago> list(Map args)

    Long count()

    void delete(Serializable id)

    MedioDePago save(MedioDePago medioDePago)

}