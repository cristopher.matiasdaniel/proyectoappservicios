package jobs

import grails.gorm.services.Service

@Service(Mensaje)
interface MensajeService {

    Mensaje get(Serializable id)

    List<Mensaje> list(Map args)

    Long count()

    void delete(Serializable id)

    Mensaje save(Mensaje mensaje)

}