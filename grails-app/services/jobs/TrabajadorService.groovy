package jobs

import grails.gorm.services.Service

@Service(Trabajador)
interface TrabajadorService {

    Trabajador get(Serializable id)
    Trabajador findByUsuario(Usuario usuario)

    List<Trabajador> list(Map args)

    Long count()

    void delete(Serializable id)

    Trabajador save(Trabajador trabajador)

}