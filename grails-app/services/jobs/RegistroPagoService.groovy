package jobs

import grails.gorm.services.Service

@Service(RegistroPago)
interface RegistroPagoService {

    RegistroPago get(Serializable id)

    List<RegistroPago> list(Map args)

    Long count()

    void delete(Serializable id)

    RegistroPago save(RegistroPago registroPago)

}