package jobs

import grails.gorm.services.Service

@Service(Servicio)
interface ServicioService {

    Servicio get(Serializable id)

    List<Servicio> list(Map args)
    //@Query("select distinct ${p} from ${Servicio p} where p.nombre = $nombre")
    List<Servicio> findAllByNombre(String nombre)
    List<Servicio> findAllByNombreIlike(String nombre)
    List<Servicio> findAllByTrabajador(Trabajador trabajador)

    Long count()

    void delete(Serializable id)

    Servicio save(Servicio servicio)


}