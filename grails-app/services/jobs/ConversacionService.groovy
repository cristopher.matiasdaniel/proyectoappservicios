package jobs

import grails.gorm.services.Service

@Service(Conversacion)
interface ConversacionService {

    Conversacion get(Serializable id)

    List<Conversacion> list(Map args)

    Long count()

    void delete(Serializable id)

    Conversacion save(Conversacion conversacion)

}