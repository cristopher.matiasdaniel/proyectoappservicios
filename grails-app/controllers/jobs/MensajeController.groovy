package jobs

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class MensajeController {

    MensajeService mensajeService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond mensajeService.list(params), model:[mensajeCount: mensajeService.count()]
    }

    def show(Long id) {
        respond mensajeService.get(id)
    }

    def create() {
        respond new Mensaje(params)
    }

    def save(Mensaje mensaje) {
        if (mensaje == null) {
            notFound()
            return
        }

        try {
            mensajeService.save(mensaje)
        } catch (ValidationException e) {
            respond mensaje.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mensaje.label', default: 'Mensaje'), mensaje.id])
                redirect mensaje
            }
            '*' { respond mensaje, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond mensajeService.get(id)
    }

    def update(Mensaje mensaje) {
        if (mensaje == null) {
            notFound()
            return
        }

        try {
            mensajeService.save(mensaje)
        } catch (ValidationException e) {
            respond mensaje.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'mensaje.label', default: 'Mensaje'), mensaje.id])
                redirect mensaje
            }
            '*'{ respond mensaje, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        mensajeService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'mensaje.label', default: 'Mensaje'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mensaje.label', default: 'Mensaje'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
