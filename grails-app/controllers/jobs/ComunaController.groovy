package jobs

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ComunaController {

    ComunaService comunaService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond comunaService.list(params), model:[comunaCount: comunaService.count()]
    }

    def show(Long id) {
        respond comunaService.get(id)
    }

    def create() {
        respond new Comuna(params)
    }

    def save(Comuna comuna) {
        if (comuna == null) {
            notFound()
            return
        }

        try {
            comunaService.save(comuna)
        } catch (ValidationException e) {
            respond comuna.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'comuna.label', default: 'Comuna'), comuna.id])
                redirect comuna
            }
            '*' { respond comuna, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond comunaService.get(id)
    }

    def update(Comuna comuna) {
        if (comuna == null) {
            notFound()
            return
        }

        try {
            comunaService.save(comuna)
        } catch (ValidationException e) {
            respond comuna.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'comuna.label', default: 'Comuna'), comuna.id])
                redirect comuna
            }
            '*'{ respond comuna, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        comunaService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'comuna.label', default: 'Comuna'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'comuna.label', default: 'Comuna'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
