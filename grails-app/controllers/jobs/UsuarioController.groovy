package jobs

import grails.validation.ValidationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.provisioning.UserDetailsManager

import static org.springframework.http.HttpStatus.*

class UsuarioController {

    UsuarioService usuarioService
    UserDetailsManager userDetailsService


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond usuarioService.list(params), model:[usuarioCount: usuarioService.count()]
    }

    def show(Long id) {
        respond usuarioService.get(id)
    }

    def showVersion2(Long id) {
        Usuario user = usuarioService.findByCorreo(session.SPRING_SECURITY_CONTEXT.authentication.principal.username)
        respond usuarioService.get(user.id)
    }

    def create() {
        respond new Usuario(params)
    }


    def save(Usuario usuario) {
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuario.fechaCreacion = new Date()
            usuario.esTrabajador = false
            usuarioService.save(usuario)

            def rol
            if(usuario.esTrabajador == true)
                rol = "TRABAJADOR"
            else
                rol = "CLIENTE"
            UserDetails userDetail = new User(usuario.getCorreo(), usuario.getContrasenia(),  [new SimpleGrantedAuthority(rol)])

            //userDetailsService.createUser(new User(it.getCorreo(), it.getContrasenia(), [new SimpleGrantedAuthority(rol)]))
            userDetailsService.createUser( userDetail)

        } catch (ValidationException e) {
            respond usuario.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.id])
                redirect usuario
            }
            '*' { respond usuario, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond usuarioService.get(id)
    }

    def update(Usuario usuario) {
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuarioService.save(usuario)
        } catch (ValidationException e) {
            respond usuario.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.id])
                redirect usuario
            }
            '*'{ respond usuario, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        usuarioService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
