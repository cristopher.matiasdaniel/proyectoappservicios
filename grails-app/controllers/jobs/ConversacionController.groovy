package jobs

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ConversacionController {

    ConversacionService conversacionService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond conversacionService.list(params), model:[conversacionCount: conversacionService.count()]
    }

    def show(Long id) {
        respond conversacionService.get(id)
    }

    def create() {
        respond new Conversacion(params)
    }

    def save(Conversacion conversacion) {
        if (conversacion == null) {
            notFound()
            return
        }

        try {
            conversacionService.save(conversacion)
        } catch (ValidationException e) {
            respond conversacion.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'conversacion.label', default: 'Conversacion'), conversacion.id])
                redirect conversacion
            }
            '*' { respond conversacion, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond conversacionService.get(id)
    }

    def update(Conversacion conversacion) {
        if (conversacion == null) {
            notFound()
            return
        }

        try {
            conversacionService.save(conversacion)
        } catch (ValidationException e) {
            respond conversacion.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'conversacion.label', default: 'Conversacion'), conversacion.id])
                redirect conversacion
            }
            '*'{ respond conversacion, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        conversacionService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'conversacion.label', default: 'Conversacion'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'conversacion.label', default: 'Conversacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
