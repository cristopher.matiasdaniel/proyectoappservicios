package jobs

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class MedioDePagoController {

    MedioDePagoService medioDePagoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond medioDePagoService.list(params), model:[medioDePagoCount: medioDePagoService.count()]
    }

    def show(Long id) {
        respond medioDePagoService.get(id)
    }

    def create() {
        respond new MedioDePago(params)
    }

    def save(MedioDePago medioDePago) {
        if (medioDePago == null) {
            notFound()
            return
        }

        try {
            medioDePagoService.save(medioDePago)
        } catch (ValidationException e) {
            respond medioDePago.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'medioDePago.label', default: 'MedioDePago'), medioDePago.id])
                redirect medioDePago
            }
            '*' { respond medioDePago, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond medioDePagoService.get(id)
    }

    def update(MedioDePago medioDePago) {
        if (medioDePago == null) {
            notFound()
            return
        }

        try {
            medioDePagoService.save(medioDePago)
        } catch (ValidationException e) {
            respond medioDePago.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'medioDePago.label', default: 'MedioDePago'), medioDePago.id])
                redirect medioDePago
            }
            '*'{ respond medioDePago, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        medioDePagoService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'medioDePago.label', default: 'MedioDePago'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'medioDePago.label', default: 'MedioDePago'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
