package jobs

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class RegistroPagoController {

    RegistroPagoService registroPagoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond registroPagoService.list(params), model:[registroPagoCount: registroPagoService.count()]
    }

    def show(Long id) {
        respond registroPagoService.get(id)
    }

    def create() {
        respond new RegistroPago(params)
    }

    def save(RegistroPago registroPago) {
        if (registroPago == null) {
            notFound()
            return
        }

        try {
            registroPagoService.save(registroPago)
        } catch (ValidationException e) {
            respond registroPago.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'registroPago.label', default: 'RegistroPago'), registroPago.id])
                redirect registroPago
            }
            '*' { respond registroPago, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond registroPagoService.get(id)
    }

    def update(RegistroPago registroPago) {
        if (registroPago == null) {
            notFound()
            return
        }

        try {
            registroPagoService.save(registroPago)
        } catch (ValidationException e) {
            respond registroPago.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'registroPago.label', default: 'RegistroPago'), registroPago.id])
                redirect registroPago
            }
            '*'{ respond registroPago, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        registroPagoService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'registroPago.label', default: 'RegistroPago'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'registroPago.label', default: 'RegistroPago'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
