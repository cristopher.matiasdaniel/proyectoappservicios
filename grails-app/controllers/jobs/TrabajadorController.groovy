package jobs

import grails.validation.ValidationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.provisioning.UserDetailsManager

import static org.springframework.http.HttpStatus.*

class TrabajadorController {

    TrabajadorService trabajadorService
    UsuarioService usuarioService
    UserDetailsManager userDetailsService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond trabajadorService.list(params), model:[trabajadorCount: trabajadorService.count()]
    }

    def show(Long id) {
        respond trabajadorService.get(id)
    }

    def showVersion2(Long id){
        Usuario user = usuarioService.findByCorreo(session.SPRING_SECURITY_CONTEXT.authentication.principal.username)
        Trabajador trabajador = trabajadorService.findByUsuario(user)
        respond trabajadorService.get(trabajador.id)
    }
    def create() {
        respond new Trabajador(params)
    }

    def createUsuarioTrabajador() {
        respond new Trabajador(params)
    }

    def save(Trabajador trabajador) {
        if (trabajador == null) {
            notFound()
            return
        }

        try {
            trabajador.usuario.fechaCreacion = new Date()
            trabajador.usuario.esTrabajador = true
            trabajador.mesesSuscrito = 0
            trabajador.cantContrataciones = 0
            trabajador.fechaUltimoPago = new Date()
            trabajador.suscrito = false

            trabajadorService.save(trabajador)

            def rol
            rol = "TRABAJADOR"
            UserDetails userDetail = new User(trabajador.usuario.getCorreo(), trabajador.usuario.getContrasenia(),  [new SimpleGrantedAuthority(rol)])

            userDetailsService.createUser( userDetail)

        } catch (ValidationException e) {
            respond trabajador.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'trabajador.label', default: 'Trabajador'), trabajador.id])
                redirect trabajador
            }
            '*' { respond trabajador, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond trabajadorService.get(id)
    }

    def update(Trabajador trabajador) {
        if (trabajador == null) {
            notFound()
            return
        }

        try {
            trabajadorService.save(trabajador)
        } catch (ValidationException e) {
            respond trabajador.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'trabajador.label', default: 'Trabajador'), trabajador.id])
                redirect trabajador
            }
            '*'{ respond trabajador, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        trabajadorService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'trabajador.label', default: 'Trabajador'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'trabajador.label', default: 'Trabajador'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
