package jobs

import com.sun.org.apache.xpath.internal.operations.Bool
import grails.validation.ValidationException

import java.awt.List

import static org.springframework.http.HttpStatus.*

class ContratacionController {

    ContratacionService contratacionService
    UsuarioService usuarioService
    TrabajadorService trabajadorService
    ServicioService servicioService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond contratacionService.list(params), model:[contratacionCount: contratacionService.count()]
    }

    def indexPorUsuario(Integer max) {
        //params.max = Math.min(max ?: 10, 100)
        Usuario user = usuarioService.findByCorreo(session.SPRING_SECURITY_CONTEXT.authentication.principal.username)
        ArrayList<Contratacion> listaContrataciones = contratacionService.findAllByUsuario(user)
        ArrayList<Contratacion> listaFiltrada = new ArrayList<Contratacion>()

        if (session.SPRING_SECURITY_CONTEXT.authentication.principal.authorities[0].toString() == "CLIENTE") {

            if (params.estado == "enProceso") {
                for (a in listaContrataciones) {
                    if (a.estado.id == 1) {
                        listaFiltrada.add(a)
                    }
                }
            } else {
                for (a in listaContrataciones) {
                    if (a.estado.id != 1) {
                        listaFiltrada.add(a)
                    }
                }
            }
        }else{
            ArrayList<Contratacion> listaCompletaPorServicios = new ArrayList<Contratacion>()
            Trabajador trabajador = trabajadorService.findByUsuario(user)
            ArrayList<Servicio> listaServicios = servicioService.findAllByTrabajador(trabajador)
            for (servicio in listaServicios){
                ArrayList<Contratacion> lista = contratacionService.findAllByServicio(servicio)
                for (contratacion in lista){
                    listaCompletaPorServicios.add(contratacion)
                }
            }
            if (params.estado == "enProceso") {
                for (a in listaCompletaPorServicios) {
                    if (a.estado.id == 1) {
                        listaFiltrada.add(a)
                    }
                }
            } else {
                for (a in listaCompletaPorServicios) {
                    if (a.estado.id != 1) {
                        listaFiltrada.add(a)
                    }
                }
            }

        }
        respond listaFiltrada, model:[contratacionCount: contratacionService.count()]
    }

    def show(Long id) {
        respond contratacionService.get(id)
    }

    def create() {
        respond new Contratacion(params)
    }

    def save(Contratacion contratacion) {
        if (contratacion == null) {
            notFound()
            return
        }

        try {
            contratacionService.save(contratacion)
        } catch (ValidationException e) {
            respond contratacion.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contratacion.label', default: 'Contratacion'), contratacion.id])
                redirect contratacion
            }
            '*' { respond contratacion, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond contratacionService.get(id)
    }

    def update(Contratacion contratacion) {
        if (contratacion == null) {
            notFound()
            return
        }

        try {
            contratacionService.save(contratacion)
        } catch (ValidationException e) {
            respond contratacion.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'contratacion.label', default: 'Contratacion'), contratacion.id])
                redirect contratacion
            }
            '*'{ respond contratacion, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        contratacionService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'contratacion.label', default: 'Contratacion'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'contratacion.label', default: 'Contratacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
