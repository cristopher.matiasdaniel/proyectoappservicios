package jobs

import javax.persistence.ManyToOne

class Conversacion {

    Long id
    Date fecha = new Date()
    Usuario usuario
    Servicio servicio

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }

    static constraints = {
        fecha display: false
    }
}
