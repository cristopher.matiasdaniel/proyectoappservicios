package jobs

import javax.persistence.ManyToOne

class Servicio {

    Long id
    String nombre
    String descripcion
    String foto
    Boolean habilitado
    Trabajador trabajador
    Direccion direccion
    Categoria categoria

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }
}
