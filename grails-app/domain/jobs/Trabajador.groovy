package jobs

import javax.persistence.ManyToOne

class Trabajador {

    Long id
    String rut
    int mesesSuscrito
    int cantContrataciones
    Date fechaUltimoPago = new Date()
    Boolean suscrito
    Usuario usuario

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }

    static constraints = {
        fechaUltimoPago display: false
    }
}
