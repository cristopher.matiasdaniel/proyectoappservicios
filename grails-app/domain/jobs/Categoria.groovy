package jobs

class Categoria {

    Long id
    String nombre
    String descripcion

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }
}
