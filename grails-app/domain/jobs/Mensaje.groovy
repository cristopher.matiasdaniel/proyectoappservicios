package jobs

import javax.persistence.ManyToOne

class Mensaje {

    Long id
    Date fecha = new Date()
    String contenido
    Conversacion conversacion

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }

    static constraints = {
        fecha display: false
    }
}
