package jobs

import javax.persistence.ManyToOne

class RegistroPago {

    Long id
    Date fecha = new Date()
    int valor
    Trabajador trabajador

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }

    static constraints = {
        fecha display: false
    }
}

