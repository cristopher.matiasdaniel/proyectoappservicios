package jobs

import javax.persistence.ManyToOne

class Contratacion {

    Long id
    Date fecha = new Date()
    int valoracion
    String comentario
    Boolean completada
    Estado estado
    Usuario usuario
    Servicio servicio

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }

    static constraints = {
        fecha display: false
    }
}
