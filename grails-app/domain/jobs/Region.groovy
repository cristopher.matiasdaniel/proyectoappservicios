package jobs

class Region {

    Long id
    String nombre

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }
}
