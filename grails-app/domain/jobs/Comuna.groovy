package jobs

import javax.persistence.ManyToOne

class Comuna {

    Long id
    String nombre
    Region region

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }
}

