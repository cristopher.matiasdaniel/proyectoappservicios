package jobs

import javax.persistence.ManyToOne

class Direccion {

    Long id
    String nombre
    int numero
    double latitud
    double longitud
    Comuna comuna

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }
}
