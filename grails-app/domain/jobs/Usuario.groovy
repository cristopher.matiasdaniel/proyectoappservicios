package jobs

class Usuario {

    Long id
    String correo
    String contrasenia
    String nombre
    String apellido
    Date fechaCreacion = new Date()
    Boolean esTrabajador

    static mapping = {
        id column: "id", type: "long", sqlType: "int", generator: 'identity'
        version false
    }
    static constraints = {
        fechaCreacion display: false
        correo email: true

    }
}
