package jobs

import org.springframework.security.provisioning.UserDetailsManager
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails

class BootStrap {
    UserDetailsManager userDetailsService
    UsuarioService usuarioService

    def init = { servletContext ->
        usuarioService.list(null).each {
            def rol = ""
            if(it.esTrabajador == true)
                rol = "TRABAJADOR"
            else
                rol = "CLIENTE"
            userDetailsService.createUser(new User(it.getCorreo(), it.getContrasenia(), [new SimpleGrantedAuthority(rol)]))
        }
        UserDetails usuario = new User('Juan', "12345", [new SimpleGrantedAuthority('ROLE_USER')])
        userDetailsService.createUser(usuario)

    }
    def destroy = {
    }
}
